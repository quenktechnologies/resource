### Build the resource package. ###
# The following variables must be defined:
# 1. PROJECT_PACKAGES_DIR

### Settings ###
TSC?=./node_modules/.bin/tsc
RESOURCE_DIR:=$(PROJECT_PACKAGES_DIR)/resource
RESOURCE_SRC_DIR:=$(RESOURCE_DIR)/src
RESOURCE_SRC_DIR_FILES:=$(shell find $(RESOURCE_SRC_DIR) -type f)

### Graph ###

# Copy all the sources to the lib folder then run tsc.
$(RESOURCE_BUILD): $(RESOURCE_SRC_DIR)
	rm -R $@ 2> /dev/null || true 
	mkdir $@
	cp -R -u $(RESOURCE_SRC_DIR)/* $@
	$(TSC) --project $@
	touch $@

$(RESOURCE_SRC_DIR): $(RESOURCE_SRC_DIR_FILES)
	touch $@
