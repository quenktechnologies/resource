import * as mongodb from 'mongodb';
import { DoFn, doN } from '@quenk/noni/lib/control/monad';
import { Object } from '@quenk/noni/lib/data/json';
import { flatten } from '@quenk/noni/lib/data/record/path';
import { ActionM } from '@quenk/tendril/lib/app/api/action';
import { Request } from '@quenk/tendril/lib/app/api/request';
import { checkout } from '@quenk/tendril/lib/app/api/action/pool';
import { await, value } from '@quenk/tendril/lib/app/api/action/control';
import { ok, created } from '@quenk/tendril/lib/app/api/action/response';
import {
    noContent,
    notFound
} from '@quenk/tendril/lib/app/api/action/response';

/**
 * Factory is a an object that can provide a model instance.
 */
export interface Factory<T extends Object, M extends Model<T>> {

    getInstance(db: mongodb.Db): M

}

/**
 * Model provides basic CRUD operations on a data model.
 */
export interface Model<T extends Object> {

    /**
     * create a new record for this model.
     */
    create(data: T): Future<Id>

    /**
     * search this model's collection using the criteria specified.
     */
    search(filter: object,opts?: object): Future<Maybe<T[]>>

    /**
     * get a single record, usually by its id.
     */
    get(id: Id, qry?: object, opts?: object): Future<Maybe<T>>

    /**
     * update a single record by its id.
     */
    update(id: Id, changes?: Partial<T>, qry?: object, opts?: object): Future<number>

    /**
     * remove a single record by its id.
     */
    remove(id: Id, qry?: object, opts?: object): Future<void>

    /**
     * count the number of records that match a query.
     */
    count(qry:object) : Future<number>

}

/**
 * WriteContext is generated after we create or updated a record.
 *
 * It captures information that can be used by subsequent actions after the
 * fact.
 */
export interface WriteContext {

    /**
     * id of the record written to.
     */
    id: string,

    /**
     * request instance used to write the value.
     */
    request: Request

}

/**
 * SearchRequest
 */
export interface SearchRequest extends Request {

    query: {

        /**
         * q is the filter object passed to find.
         */
        q: object,

        /**
         * page to return
         */
        page: number,

        /**
         * size indicates the number of records in a page
         */
        size: number,

        /**
         * sort specification
         */
        sort: object,

        /**
         * fields to include in the query
         */
        fields: object

    }

}

/**
 * create a new resource, sending the id if successful.
 */
export const create = <T extends Object, M extends Model<T>>
    (f: Factory<T, M>) => (r: Request): ActionM<WriteContext> =>
        doN(<DoFn<WriteContext, ActionM<WriteContext>>>function*() {

            let db = yield checkout('main');

            let model = f.getInstance(db);

            let id = yield await(() => model.create(r.body));

            yield created({ id });

            return value({ id, request: r });

        });

/**
 * search for values that match the submitted query params.
 */
export const search = <T extends Object, M extends Model<T>>
    (f: Factory<T, M>) => (r: SearchRequest): ActionM<undefined> =>
        doN(<DoFn<undefined, ActionM<undefined>>>function*() {

            let { page, size, q, sort, fields } = r.query;

            let projection = fields;

            let db = yield checkout('main');

            let model = f.getInstance(db);

            let n = yield await(() => model.count(r.query.q));

            let total = Math.ceil(n / size);

            let current =
                ((total === 0) || (page <= 0)) ? 0 : (page >= total) ?
                    total - 1 : page;

            let skip = current * size;

            let o = { skip, limit: size, sort, projection };

            let maybeData = yield await(() => model.search(q, o));

            if (maybeData.isJust()) {

                let data = maybeData.get();

                let payload = { data, pages: { current, size, total } };

                return ok(payload);

            } else {

                return noContent();

            }

        });

/**
 * update
 */
export const update = <T extends Object, M extends Model<T>>
    (f: Factory<T, M>) => (r: Request): ActionM<WriteContext> =>
        doN(<DoFn<WriteContext, ActionM<WriteContext>>>function*() {

            let id = r.params.id;

            let changes = r.body;

            let db = yield checkout('main');

            let model = f.getInstance(db);

            let n = yield await(() =>
                model.update(id, <object>flatten(changes), r.query));

            yield (n !== 0) ? ok({}) : notFound();

            return value({ id, request: r });

        });

/**
 * get a single value given its id.
 */
export const get = <T extends Object, M extends Model<T>>
    (f: Factory<T, M>) => (r: Request): ActionM<undefined> =>
        doN(<DoFn<undefined, ActionM<undefined>>>function*() {

            let db = yield checkout('main');

            let model = f.getInstance(db);

            let maybeData = yield await(() => model.get(r.params.id, r.query));

            if (maybeData.isJust()) {

                let data = maybeData.get();

                return ok({ data });

            } else {

                return notFound();

            }

        });

/**
 * remove
 */
export const remove = <T extends Object, M extends Model<T>>
    (f: Factory<T, M>) => (r: Request): ActionM<undefined> =>
        doN(<DoFn<undefined, ActionM<undefined>>>function*() {

            let db = yield checkout('main');

            let model = f.getInstance(db);

            yield await(() => model.remove(r.params.id, r.query));

            return ok({});

        });
